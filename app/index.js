// First, require React and other needed modules
var React = require('react'),
    ReactDOM = require('react-dom');

/*=== BPM input component ===*/
function StartButton(props) {
  return (
    <button onClick={props.onClick}>Start</button>
  );
}

function StopButton(props) {
  return (
    <button onClick={props.onClick}>Stop</button>
  );
}

class Metronome extends React.Component {
  constructor(props) {
    super(props);
    // Link the functions to the component itself
    this.onSliderChange = this.onSliderChange.bind(this);
    this.startMetronome = this.startMetronome.bind(this);
    this.stopMetronome = this.stopMetronome.bind(this);
    this.updateMetronome = this.updateMetronome.bind(this);
    this.pulseOnBeat = this.pulseOnBeat.bind(this);
    this.state = {
      beat: 0,
      beatDivision: 4,
      isPlaying: false,
      timerID: null,
      value: 60,
    };
  }

  componentDidUpdate() {
    // if (this.state.isPlaying) {
    //   this.startMetronome();
    // } else {
    //   this.stopMetronome();
    // }
  }

  onSliderChange(e) {
    this.setState({
      value: e.target.value
    });

    this.updateMetronome();
  }

  updateMetronome() {
    if (!this.state.isPlaying) { return; }

    window.clearTimeout(this.timerID);
    this.startMetronome();
  }

  startMetronome() {
    this.setState({
      isPlaying: true
    });

    this.timerID = setInterval(
      () => this.pulseOnBeat(),
      60 / this.state.value * 1000
    );
  }

  stopMetronome() {
    this.setState({
      isPlaying: false
    });

    window.clearTimeout(this.timerID);
  }

  pulseOnBeat() {
    this.setState({
      beat: (this.state.beat++ % this.state.beatDivision + 1)
    });
  }


  render() {
    let button = null;
    if (!this.state.isPlaying) {
      button = <StartButton onClick={this.startMetronome}/>;
    } else {
      button = <StopButton onClick={this.stopMetronome}/>;
    }

    return (
      <div id='metronome'>
        <h1>React Metronome</h1>
        <div className='display'>{this.state.beat}</div>
        <div className='bpm-control'>
          <div id='bpm'>{this.state.value}</div>
          <div className='bpm-slider'>
            <span>40</span>
            <input
              type="range"
              id="bpm-slider"
              min="40"
              max="200"
              value={this.state.value}
              onChange={this.onSliderChange} />
            <span>200</span>
          </div>
          {button}
        </div>
      </div>
    );
  }
}




ReactDOM.render(
  <Metronome />,
  document.getElementById('app')
);
