// First off, require the WebPack HTML Plugin
var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
  template: __dirname + '/app/index.html', // use the index.html in ./app
  filename: 'index.html', // output file name
  inject: 'body' // inject JS at the end of the body instead of the head
});

module.exports = {
  entry: [
    './app/index.js'
  ],
  output: {
    path: __dirname + '/dist',
    filename: 'index_bundle.js'
  },
  // So module -> loaders is describing what code-processing-tool we want to
  // use and what files to put through that tool
  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'}
    ]
  },
  plugins: [HTMLWebpackPluginConfig]
};
